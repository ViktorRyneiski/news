import by.itstep.entity.HwCustomer;
import by.itstep.repository.SimpleHwCustomerRepository;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class TestSimpleHwCustomerRepository {

    SimpleHwCustomerRepository simpleHwCustomerRepository = new SimpleHwCustomerRepository();

    @Test
    public void getById_happyPath() {
        //GIVEN
        HwCustomer testCustomer = HwCustomer.getBuilder()
                .id(1L)
                .firstName("Viktor")
                .lastName("Ryneiski")
                .address("Kalinovskogo 64/11")
                .birthDate(new Date(87, 11, 12)) //косяк с датос(с 1900)
                .city("Minsk")
                .state("DV")
                .phone("80296649098")
                .points(162)
                .build();

        //WHEN
        HwCustomer foundHwCustomer = simpleHwCustomerRepository.getById(1L);

        //THEN
        Assert.assertEquals(testCustomer, foundHwCustomer);
    }

    @Test
    public void findAll_happyPath() {
        //GIVEN
        HwCustomer c1 = HwCustomer.getBuilder()
                .id(1L)
                .firstName("Viktor")
                .lastName("Ryneiski")
                .address("Kalinovskogo 64/11")
                .birthDate(new Date(87, 11, 12)) //косяк с датос(с 1900)
                .city("Minsk")
                .state("DV")
                .phone("80296649098")
                .points(162)
                .build();

        HwCustomer c2 = HwCustomer.getBuilder()
                .id(2L)
                .firstName("Gena")
                .lastName("Ryneiski")
                .address("PoludPołud 15")
                .birthDate(null) //косяк с датос(с 1900)
                .city("Szczecin")
                .state("DV")
                .phone("+48514248289")
                .points(155)
                .build();

        HwCustomer c3 = HwCustomer.getBuilder()
                .id(4L)
                .firstName("John")
                .lastName("Johnson")
                .address("Kalinowskogo 64-11")
                .birthDate(null) //косяк с датос(с 1900)
                .city("Minsk")
                .state("BG")
                .phone("0296649098")
                .points(325)
                .build();

        List<HwCustomer> testHwCustomers = new ArrayList<>();

        testHwCustomers.add(c1);
        testHwCustomers.add(c2);
        testHwCustomers.add(c3);

        //WHEN
        List<HwCustomer> foundHwCustomers = simpleHwCustomerRepository.findAll();

        //THEN
        Assert.assertEquals(testHwCustomers, foundHwCustomers);
    }

    @Test
    public void save_happyPath() {
        //GIVEN
        HwCustomer testHwCustomer = new HwCustomer();
        testHwCustomer.setFirstName("John");
        testHwCustomer.setLastName("Johnson");
        testHwCustomer.setAddress("Kalinowskogo 64-11");
        testHwCustomer.setBirthDate(new Date(2000, 11, 11));
        testHwCustomer.setCity("Minsk");
        testHwCustomer.setPhone("80296649098");
        testHwCustomer.setPoints(325);
        testHwCustomer.setState("BG");

        //WHEN
        simpleHwCustomerRepository.save(testHwCustomer);
        List<HwCustomer> hwCustomers = simpleHwCustomerRepository.findAll();

        //THEN
        Assert.assertTrue(hwCustomers.contains(testHwCustomer));
    }

    @Test
    public void update_happyPath(){

    }

    @Test
    public void delete_happyPath(){

    }
}
