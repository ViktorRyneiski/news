package by.itstep.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shippers")
public class HwShipper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "hwShipper",fetch = FetchType.EAGER)
    private List<HwOrder> hwOrders;


    public HwShipper() {
    }

    public HwShipper(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public static Builder getBuilder(){
        return new Builder();
    }

    public static class Builder{

        private HwShipper hwShipper = new HwShipper();

        public Builder id(Long id){
            hwShipper.id = id;
            return this;
        }

        public Builder name(String name){
            hwShipper.name = name;
            return this;
        }

        public HwShipper build(){
            return hwShipper;
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final HwShipper hwShipper = (HwShipper) o;
        return id == hwShipper.id &&
                Objects.equals(name, hwShipper.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Shipper{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
