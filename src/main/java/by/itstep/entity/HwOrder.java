package by.itstep.entity;

import java.sql.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class HwOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

//    @Column(name = "customer_id")
//    private int customerId;

    @Column(name = "order_date")
    private Date orderDate;

    @Column(name = "status")
    private int status;

    @Column(name = "comments")
    private String comments;

    @Column(name = "shipped_date")
    private Date shippedDate;

//    @Column(name = "shipper_id")
//    private int shipperId;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private HwCustomer hwCustomer;

    @OneToMany(mappedBy = "hwOrder")
    private List<HwOrderItem> hwOrderItems;

    @ManyToOne
    @JoinColumn(name = "shipper_id")
    private HwShipper hwShipper;


    public HwOrder() {
    }

    public HwOrder(final Date orderDate, final int status, final String comments, final Date shippedDate,
                   final HwCustomer hwCustomer, final List<HwOrderItem> hwOrderItems, final HwShipper hwShipper) {
        this.orderDate = orderDate;
        this.status = status;
        this.comments = comments;
        this.shippedDate = shippedDate;
        this.hwCustomer = hwCustomer;
        this.hwOrderItems = hwOrderItems;
        this.hwShipper = hwShipper;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(final Long orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(final Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(final String comments) {
        this.comments = comments;
    }

    public Date getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(final Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    public HwCustomer getHwCustomer() {
        return hwCustomer;
    }

    public void setHwCustomer(final HwCustomer hwCustomer) {
        this.hwCustomer = hwCustomer;
    }

    public List<HwOrderItem> getHwOrderItems() {
        return hwOrderItems;
    }

    public void setHwOrderItems(final List<HwOrderItem> hwOrderItems) {
        this.hwOrderItems = hwOrderItems;
    }

    public HwShipper getHwShipper() {
        return hwShipper;
    }

    public void setHwShipper(final HwShipper hwShipper) {
        this.hwShipper = hwShipper;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final HwOrder hwOrder = (HwOrder) o;
        return orderId == hwOrder.orderId &&
                status == hwOrder.status &&
                Objects.equals(orderDate, hwOrder.orderDate) &&
                Objects.equals(comments, hwOrder.comments) &&
                Objects.equals(shippedDate, hwOrder.shippedDate) &&
                Objects.equals(hwCustomer, hwOrder.hwCustomer) &&
                Objects.equals(hwOrderItems, hwOrder.hwOrderItems) &&
                Objects.equals(hwShipper, hwOrder.hwShipper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderDate, status, comments, shippedDate, hwCustomer, hwOrderItems, hwShipper);
    }

    @Override
    public String toString() {
        return "HwOrder{" +
                "orderId=" + orderId +
                ", orderDate=" + orderDate +
                ", status=" + status +
                ", comments='" + comments + '\'' +
                ", shippedDate=" + shippedDate +
                ", hwCustomer=" + hwCustomer +
                ", hwOrderItems=" + hwOrderItems +
                ", hwShipper=" + hwShipper +
                '}';
    }
}
