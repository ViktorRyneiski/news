package by.itstep.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class HwProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity_in_stock")
    private int quantityInStock;

    @Column(name = "unit_price")
    private double unitPrice;

    @OneToMany(mappedBy = "hwProduct", fetch = FetchType.EAGER)
    private List<HwOrderItem> hwOrderItems;

    public HwProduct() {
    }

    public HwProduct(final Long id, final String name, final int quantityInStock, final double unitPrice) {
        this.id = id;
        this.name = name;
        this.quantityInStock = quantityInStock;
        this.unitPrice = unitPrice;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(final int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(final double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final HwProduct hwProduct = (HwProduct) o;
        return id == hwProduct.id &&
                quantityInStock == hwProduct.quantityInStock &&
                Double.compare(hwProduct.unitPrice, unitPrice) == 0 &&
                Objects.equals(name, hwProduct.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantityInStock, unitPrice);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantityInStock=" + quantityInStock +
                ", unitPrice=" + unitPrice +
                '}';
    }

    public static class Builder {

        private HwProduct hwProduct = new HwProduct();

        public Builder id(Long id) {
            hwProduct.id = id;
            return this;
        }

        public Builder name(String name) {
            hwProduct.name = name;
            return this;
        }

        public Builder quantityInStock(int quantityInStock) {
            hwProduct.quantityInStock = quantityInStock;
            return this;
        }

        public Builder unitPrise(double unitPrice) {
            hwProduct.unitPrice = unitPrice;
            return this;
        }

        public HwProduct build() {
            return hwProduct;
        }
    }
}
