package by.itstep.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.zip.CheckedOutputStream;


@Entity // <- Говорим что этот класс является таблицей в БД
@Table(name = "news") // <- Название таблицы
public class News {

    @Id // <- Primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) // <- auto increment
    private Long id;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "header") // <- это столбик
    private String header;

    @Column(name = "content")
    private String content;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "published")
    private boolean published;

    @OneToMany(mappedBy = "news", fetch = FetchType.EAGER)
    private List<Comment> comments;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "news_tag",
            joinColumns = {@JoinColumn (name = "news_id")},
            inverseJoinColumns = {@JoinColumn (name = "tag_id")}
    )
    private List<Tag> tags;

    public News() {
    }

    public News(Long id, String header, String content,
                Date createdAt, boolean published) {
        this.id = id;
        this.header = header;
        this.content = content;
        this.createdAt = createdAt;
        this.published = published;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(final List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(final List<Comment> comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return published == news.published &&
                Objects.equals(id, news.id) &&
                Objects.equals(header, news.header) &&
                Objects.equals(content, news.content) &&
                Objects.equals(createdAt, news.createdAt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, header, content, createdAt, published);
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", header='" + header + '\'' +
                ", content='" + content + '\'' +
                ", createdAt=" + createdAt +
                ", published=" + published +
                '}';
    }

}
