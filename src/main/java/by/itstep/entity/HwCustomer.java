package by.itstep.entity;

import java.sql.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class HwCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "phone")
    private String phone;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "points")
    private int points;

    @OneToMany(mappedBy = "hwCustomer", fetch = FetchType.EAGER)
    private List<HwOrder> hwOrders;

    public HwCustomer() {
    }

    public HwCustomer(final Long id, final String first_name, final String last_name, final Date birth_date, final String phone,
                    final String address, final String city, final String state, final int points) {
        this.id = id;
        this.firstName = first_name;
        this.lastName = last_name;
        this.birthDate = birth_date;
        this.phone = phone;
        this.address = address;
        this.city = city;
        this.state = state;
        this.points = points;
    }

    public static Builder getBuilder(){
        return new Builder();
    }

    public static class Builder{
        private HwCustomer customer = new HwCustomer();

        public Builder id(Long id){
            customer.id = id;
            return this;
        }

        public Builder firstName(String firstName){
            customer.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName){
            customer.lastName = lastName;
            return this;
        }

        public Builder birthDate(Date birthDate){
            customer.birthDate = birthDate;
            return this;
        }

        public Builder phone(String phone){
            customer.phone = phone;
            return this;
        }

        public Builder address(String address){
            customer.address = address;
            return this;
        }

        public Builder city(String city){
            customer.city = city;
            return this;
        }

        public Builder state(String state){
            customer.state = state;
            return this;
        }

        public Builder points(int points){
            customer.points = points;
            return this;
        }

        public HwCustomer build(){
            return customer;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(final int points) {
        this.points = points;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final HwCustomer customer = (HwCustomer) o;
        return id == customer.id &&
                points == customer.points &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(birthDate, customer.birthDate) &&
                Objects.equals(phone, customer.phone) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(city, customer.city) &&
                Objects.equals(state, customer.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, birthDate, phone, address, city, state, points);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", birth_date=" + birthDate +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", points=" + points +
                '}';
    }
}
