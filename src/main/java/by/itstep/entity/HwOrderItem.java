package by.itstep.entity;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "order_items")
public class HwOrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Column(name = "order_id")
//    private int orderId;
//
//    @Column(name = "product_id")
//    private int productId;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "unit_price")
    private double unitPrice;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private HwOrder hwOrder;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private HwProduct hwProduct;

    public HwOrderItem() {
    }

    public HwOrderItem(final int quantity, final double unitPrice, final HwOrder hwOrder, final HwProduct hwProduct) {
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.hwOrder = hwOrder;
        this.hwProduct = hwProduct;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(final double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public HwOrder getHwOrder() {
        return hwOrder;
    }

    public void setHwOrder(final HwOrder hwOrder) {
        this.hwOrder = hwOrder;
    }

    public HwProduct getHwProduct() {
        return hwProduct;
    }

    public void setHwProduct(final HwProduct hwProduct) {
        this.hwProduct = hwProduct;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final HwOrderItem that = (HwOrderItem) o;
        return quantity == that.quantity &&
                Double.compare(that.unitPrice, unitPrice) == 0 &&
                Objects.equals(id, that.id) &&
                Objects.equals(hwOrder, that.hwOrder) &&
                Objects.equals(hwProduct, that.hwProduct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, quantity, unitPrice, hwOrder, hwProduct);
    }

    @Override
    public String toString() {
        return "HwOrderItem{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                ", hwOrder=" + hwOrder +
                ", hwProduct=" + hwProduct +
                '}';
    }
}
