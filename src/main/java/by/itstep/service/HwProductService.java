package by.itstep.service;

import by.itstep.entity.HwProduct;

import java.util.List;

public interface HwProductService {

    HwProduct getById(Long id);

    List<HwProduct> findAll();

    void save(HwProduct hwProduct);

    void update(HwProduct hwProduct);

    void delete(Long id);
}
