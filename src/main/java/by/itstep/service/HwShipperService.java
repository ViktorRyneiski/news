package by.itstep.service;

import by.itstep.entity.HwShipper;

import java.util.List;

public interface HwShipperService {

    HwShipper getById(Long id);

    List<HwShipper> findAll();

    void save(HwShipper hwShipper);

    void update(HwShipper hwShipper);

    void delete(Long id);
}
