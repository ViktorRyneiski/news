package by.itstep.service;

import by.itstep.entity.HwOrderItem;

import java.util.List;

public interface HwOrderItemService {

    HwOrderItem getById(Long id);

    List<HwOrderItem> findAll();

    void save(HwOrderItem hwOrderItem);

    void update(HwOrderItem hwOrderItem);

    void delete(Long id);
}
