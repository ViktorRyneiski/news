package by.itstep.service;

import by.itstep.entity.News;
import by.itstep.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleNewsService implements NewsService {

    @Autowired
    private NewsRepository newsRepository;

    public List<News> findAll() {
        System.out.println("Service -> findAll");
        List<News> foundNews = newsRepository.findAll();
        System.out.println("Service: found " + foundNews.size() + " news...");

        return foundNews;
    }

    public News findById(Long id) {
        System.out.println("Service -> find by id: " + id);
        News foundNews = newsRepository.findById(id);
        System.out.println("Service -> found: " + foundNews);

        return foundNews;
    }

    public News save(News news) {
        System.out.println("Service -> before save");
        News newsToSave = newsRepository.save(news);
        System.out.println("Service -> after save");
        return newsToSave;
    }

}
