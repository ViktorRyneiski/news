package by.itstep.service;

import by.itstep.entity.Comment;
import by.itstep.entity.News;

import java.util.List;

public interface CommentService {

    List<Comment> findAll();

    Comment findById(Long id);

    void save(Comment comment);
}
