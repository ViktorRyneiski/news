package by.itstep.service;

import by.itstep.entity.News;

import java.util.List;

public interface NewsService {

    List<News> findAll();

    News findById(Long id);

    News save(News news);

}
