package by.itstep.service;

import by.itstep.entity.HwCustomer;

import java.util.List;

public interface HwCustomerService {

    HwCustomer getById(Long id);

    List<HwCustomer> findAll();

    void save(HwCustomer hwCustomer);

    void update(HwCustomer hwCustomer);

    void delete(Long id);
}
