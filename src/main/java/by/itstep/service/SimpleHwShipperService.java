package by.itstep.service;

import by.itstep.entity.HwShipper;
import by.itstep.repository.HwShipperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHwShipperService implements HwShipperService{

    @Autowired
    HwShipperRepository hwShipperRepository;

    @Override
    public HwShipper getById(final Long id) {
        return hwShipperRepository.getById(id);
    }

    @Override
    public List<HwShipper> findAll() {
        return hwShipperRepository.findAll();
    }

    @Override
    public void save(final HwShipper hwShipper) {
        hwShipperRepository.save(hwShipper);
    }

    @Override
    public void update(final HwShipper hwShipper) {
        hwShipperRepository.update(hwShipper);
    }

    @Override
    public void delete(final Long id) {
        hwShipperRepository.delete(id);
    }
}
