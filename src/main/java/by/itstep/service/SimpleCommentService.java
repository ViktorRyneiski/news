package by.itstep.service;

import by.itstep.entity.Comment;
import by.itstep.repository.CommentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleCommentService implements CommentService {

    private static final Logger logger = LogManager.getLogger(SimpleCommentService.class);

    @Autowired
    CommentRepository commentRepository;

    @Override
    public List<Comment> findAll() {
        logger.debug("Service -> findAll {}", 12324);
        List<Comment> foundComment = commentRepository.findAll();
        logger.debug("Service: found " + foundComment.size() + " news...");
        return foundComment;
    }

    @Override
    public Comment findById(final Long id) {
        logger.debug("Service -> find by id: " + id);
        Comment foundComment = commentRepository.getById(id);
        logger.debug("Service -> found: " + foundComment);
        return foundComment;
    }

    @Override
    public void save(final Comment comment) {
        System.out.println("Service -> before save");
        commentRepository.save(comment);
        System.out.println("Service -> after save");
    }
}
