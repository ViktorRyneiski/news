package by.itstep.service;

import by.itstep.entity.HwCustomer;
import by.itstep.repository.HwCustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHwCustomerService implements HwCustomerService {

    @Autowired
    HwCustomerRepository hwCustomerRepository;

    @Override
    public HwCustomer getById(final Long id) {
        return hwCustomerRepository.getById(id);
    }

    @Override
    public List<HwCustomer> findAll() {
        return hwCustomerRepository.findAll();
    }

    @Override
    public void save(final HwCustomer hwCustomer) {
        hwCustomerRepository.save(hwCustomer);
    }

    @Override
    public void update(final HwCustomer hwCustomer) {
        hwCustomerRepository.update(hwCustomer);
    }

    @Override
    public void delete(final Long id) {
        hwCustomerRepository.delete(id);
    }
}
