package by.itstep.service;

import by.itstep.entity.HwOrder;
import by.itstep.entity.HwProduct;
import by.itstep.repository.HwProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHwProductService implements HwProductService{

    @Autowired
    HwProductRepository hwProductRepository;

    @Override
    public HwProduct getById(final Long id) {
        return hwProductRepository.getById(id);
    }

    @Override
    public List<HwProduct> findAll() {
        return hwProductRepository.findAll();
    }

    @Override
    public void save(final HwProduct hwProduct) {
        hwProductRepository.save(hwProduct);
    }

    @Override
    public void update(final HwProduct hwProduct) {
        hwProductRepository.update(hwProduct);
    }

    @Override
    public void delete(final Long id) {
        hwProductRepository.delete(id);
    }
}
