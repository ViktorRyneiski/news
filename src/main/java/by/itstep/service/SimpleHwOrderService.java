package by.itstep.service;

import by.itstep.entity.HwOrder;
import by.itstep.repository.HwOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHwOrderService implements HwOrderService {

    @Autowired
    HwOrderRepository hwOrderRepository;

    @Override
    public HwOrder getById(final Long id) {
        HwOrder foundHwOrder = hwOrderRepository.getById(id);
        return foundHwOrder;
    }

    @Override
    public List<HwOrder> findAll() {
        List<HwOrder> foundHwOrders = hwOrderRepository.findAll();
        return foundHwOrders;
    }

    @Override
    public void save(final HwOrder hwOrder) {
        hwOrderRepository.save(hwOrder);
    }

    @Override
    public void update(final HwOrder hwOrder) {
        hwOrderRepository.update(hwOrder);
    }

    @Override
    public void delete(final Long id) {
        hwOrderRepository.delete(id);
    }
}
