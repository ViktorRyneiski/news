package by.itstep.service;

import by.itstep.entity.HwOrderItem;
import by.itstep.repository.HwOrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleHwOrderItemService implements HwOrderItemService {

    @Autowired
    HwOrderItemRepository hwOrderItemRepository;

    @Override
    public HwOrderItem getById(final Long id) {
        HwOrderItem foundHwOrderItem = hwOrderItemRepository.getById(id);
        return foundHwOrderItem;
    }

    @Override
    public List<HwOrderItem> findAll() {
        List<HwOrderItem> findOrderItems = hwOrderItemRepository.findAll();
        return findOrderItems;
    }

    @Override
    public void save(final HwOrderItem hwOrderItem) {
        hwOrderItemRepository.save(hwOrderItem);
    }

    @Override
    public void update(final HwOrderItem hwOrderItem) {
        hwOrderItemRepository.update(hwOrderItem);
    }

    @Override
    public void delete(final Long id) {
        hwOrderItemRepository.delete(id);
    }
}
