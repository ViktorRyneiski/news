package by.itstep.service;

import by.itstep.entity.HwOrder;

import java.util.List;

public interface HwOrderService {

    HwOrder getById(Long id);

    List<HwOrder> findAll();

    void save(HwOrder hwOrder);

    void update(HwOrder hwOrder);

    void delete(Long id);
}
