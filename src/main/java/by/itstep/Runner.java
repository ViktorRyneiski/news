package by.itstep;

import by.itstep.controller.NewsController;
import by.itstep.util.EntityManagerUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);

        EntityManagerUtils.getEntityManager();
    }

}
