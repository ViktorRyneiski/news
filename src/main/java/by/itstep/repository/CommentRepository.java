package by.itstep.repository;

import by.itstep.entity.Comment;

import java.util.List;

public interface CommentRepository {

    Comment getById(Long id);

    List<Comment> findAll();

    void save(Comment comment);

    void update(Comment comment);

    void delete(Long id);

    void increaseRating(Long commentId);

    void decreaseRating(Long commentId);
}
