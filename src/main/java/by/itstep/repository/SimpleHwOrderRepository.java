package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.HwOrder;
import by.itstep.entity.HwOrderItem;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleHwOrderRepository implements HwOrderRepository{

    @Override
    public HwOrder getById(final Long id) {
        EntityManager em = getEntityManager();

        HwOrder foundHwOrder = em.find(HwOrder.class, id);

        em.close();

        return foundHwOrder;
    }

    @Override
    public List<HwOrder> findAll() {
        EntityManager em = getEntityManager();

        List<HwOrder> foundHwOrder = em.createQuery("From HwOrder", HwOrder.class)
                .getResultList();

        em.close();

        return foundHwOrder;
    }

    @Override
    public void save(final HwOrder hwOrder) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(hwOrder);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final HwOrder hwOrder) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        HwOrder hwOrderToUpdate = em.find(HwOrder.class, hwOrder.getOrderId());

        hwOrderToUpdate.setComments(hwOrder.getComments());
        hwOrderToUpdate.setHwCustomer(hwOrder.getHwCustomer());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        HwOrder hwOrderToDelete = em.find(HwOrder.class, id);

        em.remove(hwOrderToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
