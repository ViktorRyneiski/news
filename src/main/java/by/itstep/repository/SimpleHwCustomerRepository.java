package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.Comment;
import by.itstep.entity.HwCustomer;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleHwCustomerRepository implements HwCustomerRepository {

    @Override
    public HwCustomer getById(final Long id) {
        EntityManager em = getEntityManager();

        HwCustomer foundHwCustomer = em.find(HwCustomer.class, id);

        em.close();

        return foundHwCustomer;
    }

    @Override
    public List<HwCustomer> findAll() {
        EntityManager em = getEntityManager();

        List<HwCustomer> foundHwCustomers = em.createQuery("From HwCustomer", HwCustomer.class)
                .getResultList();

        em.close();

        return foundHwCustomers;
    }

    @Override
    public void save(final HwCustomer hwCustomer) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(hwCustomer);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final HwCustomer hwCustomer) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        HwCustomer hwCustomerToUpdate = em.find(HwCustomer.class, hwCustomer.getId());

        hwCustomerToUpdate.setFirstName(hwCustomer.getFirstName());
        hwCustomerToUpdate.setLastName(hwCustomer.getLastName());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        HwCustomer hwCustomerToDelete = em.find(HwCustomer.class, id);

        em.remove(hwCustomerToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
