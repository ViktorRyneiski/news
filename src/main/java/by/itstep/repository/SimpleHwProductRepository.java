package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.HwOrder;
import by.itstep.entity.HwProduct;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleHwProductRepository implements HwProductRepository {

    @Override
    public HwProduct getById(final Long id) {
        EntityManager em = getEntityManager();

        HwProduct foundHwProduct = em.find(HwProduct.class, id);

        em.close();

        return foundHwProduct;
    }

    @Override
    public List<HwProduct> findAll() {
        EntityManager em = getEntityManager();

        List<HwProduct> foundHwProduct = em.createQuery("From HwProduct", HwProduct.class)
                .getResultList();

        em.close();

        return foundHwProduct;
    }

    @Override
    public void save(final HwProduct hwProduct) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(hwProduct);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final HwProduct hwProduct) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        HwProduct hwProductToUpdate = em.find(HwProduct.class, hwProduct.getId());

        hwProductToUpdate.setName(hwProduct.getName());
        hwProductToUpdate.setUnitPrice(hwProduct.getUnitPrice());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        HwProduct hwProductToDelete = em.find(HwProduct.class, id);

        em.remove(hwProductToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
