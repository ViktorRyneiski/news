package by.itstep.repository;

import by.itstep.entity.User;

import java.util.List;

public interface UserRepository {

    User getById(Long id);

    List<User> findAll();

    void save(User user);

    void update(User user);

    void delete(Long id);

}
