package by.itstep.repository;

import by.itstep.entity.Tag;

import java.util.List;

public interface TagRepository {

    Tag getById(Long id);

    List<Tag> findAll();

    void save(Tag tag);

    void update(Tag tag);

    void delete(Long id);

}
