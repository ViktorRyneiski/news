package by.itstep.repository;

import by.itstep.entity.HwShipper;

import java.util.List;

public interface HwShipperRepository {

    HwShipper getById(Long id);

    List<HwShipper> findAll();

    void save(HwShipper hwShipper);

    void update(HwShipper hwShipper);

    void delete(Long id);
}
