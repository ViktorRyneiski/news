package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.HwProduct;
import by.itstep.entity.HwShipper;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleHwShipperRepository implements HwShipperRepository{

    @Override
    public HwShipper getById(final Long id) {
        EntityManager em = getEntityManager();

        HwShipper foundHwShipper = em.find(HwShipper.class, id);

        em.close();

        return foundHwShipper;
    }

    @Override
    public List<HwShipper> findAll() {
        EntityManager em = getEntityManager();

        List<HwShipper> foundHwShipper = em.createQuery("From HwShipper", HwShipper.class)
                .getResultList();

        em.close();

        return foundHwShipper;
    }

    @Override
    public void save(final HwShipper hwShipper) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(hwShipper);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final HwShipper hwShipper) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        HwShipper hwShipperToUpdate = em.find(HwShipper.class, hwShipper.getId());

        hwShipperToUpdate.setName(hwShipper.getName());
        hwShipperToUpdate.setId(hwShipper.getId());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        HwShipper HwShipperToDelete = em.find(HwShipper.class, id);

        em.remove(HwShipperToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
