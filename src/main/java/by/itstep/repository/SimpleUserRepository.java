package by.itstep.repository;

import by.itstep.entity.User;
import by.itstep.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class SimpleUserRepository implements UserRepository {

    @Override
    public User getById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User foundUser = em.find(User.class, id);

        em.close();

        return foundUser;
    }

    @Override
    public List<User> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<User> foundUsers = em.createNativeQuery("SELECT * FROM user")
                .getResultList();

        em.close();

        return foundUsers;
    }

    @Override
    public void save(final User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(user);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final User user) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        User userToUpdate = em.find(User.class, user.getId());

        userToUpdate.setImageUrl(user.getImageUrl());
        userToUpdate.setRole(user.getRole());
        userToUpdate.setUserName(user.getUserName());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        User userToDelete = em.find(User.class, id);
        em.remove(userToDelete);

        em.getTransaction().commit();
        em.close();

    }
}
