package by.itstep.repository;

import by.itstep.entity.News;

import java.util.List;

public interface NewsRepository {

    List<News> findAll();

    News findById(Long id);

    News save(News news);

    void update(News news);

    void delete(Long id);

}
