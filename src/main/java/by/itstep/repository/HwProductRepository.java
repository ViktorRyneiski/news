package by.itstep.repository;

import by.itstep.entity.HwProduct;

import java.util.List;

public interface HwProductRepository {

    HwProduct getById(Long id);

    List<HwProduct> findAll();

    void save(HwProduct hwProduct);

    void update(HwProduct hwProduct);

    void delete(Long id);
}
