package by.itstep.repository;

import by.itstep.entity.Comment;
import by.itstep.entity.HwCustomer;

import java.util.List;

public interface HwCustomerRepository {

    HwCustomer getById(Long id);

    List<HwCustomer> findAll();

    void save(HwCustomer hwCustomer);

    void update(HwCustomer hwCustomer);

    void delete(Long id);
}
