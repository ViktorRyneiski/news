package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.HwCustomer;
import by.itstep.entity.HwOrderItem;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleHwOrderItemRepository implements HwOrderItemRepository {

    @Override
    public HwOrderItem getById(final Long id) {
        EntityManager em = getEntityManager();

        HwOrderItem foundHwOrderItem = em.find(HwOrderItem.class, id);

        em.close();

        return foundHwOrderItem;
    }

    @Override
    public List<HwOrderItem> findAll() {
        EntityManager em = getEntityManager();

        List<HwOrderItem> foundHwOrderItem = em.createQuery("From HwOrderItem", HwOrderItem.class)
                .getResultList();

        em.close();

        return foundHwOrderItem;
    }

    @Override
    public void save(final HwOrderItem hwOrderItem) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(hwOrderItem);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final HwOrderItem hwOrderItem) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        HwOrderItem hwOrderItemToUpdate = em.find(HwOrderItem.class, hwOrderItem.getId());

        hwOrderItemToUpdate.setQuantity(hwOrderItem.getQuantity());
        hwOrderItemToUpdate.setHwOrder(hwOrderItem.getHwOrder());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        HwOrderItem hwOrderItemToDelete = em.find(HwOrderItem.class, id);

        em.remove(hwOrderItemToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
