package by.itstep.repository;

import by.itstep.entity.HwCustomer;
import by.itstep.entity.HwOrder;

import java.util.List;

public interface HwOrderRepository {

    HwOrder getById(Long id);

    List<HwOrder> findAll();

    void save(HwOrder hwOrder);

    void update(HwOrder hwOrder);

    void delete(Long id);
}
