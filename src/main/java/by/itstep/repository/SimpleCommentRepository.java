package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.Comment;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimpleCommentRepository implements CommentRepository {

    @Override
    public Comment getById(final Long id) {
        EntityManager em = getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();

        return foundComment;
    }

    @Override
    public List<Comment> findAll() {
        EntityManager em = getEntityManager();

        List<Comment> foundComments = em.createNativeQuery("SELECT * FROM comment")
                .getResultList();

        em.close();

        return foundComments;
    }

    @Override
    public void save(final Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Comment commentToUpdate = em.find(Comment.class, comment.getId());

        commentToUpdate.setMessage(comment.getMessage());
        commentToUpdate.setRating(comment.getRating());

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment commentToDelete = em.find(Comment.class, id);

        em.remove(commentToDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void increaseRating(final Long commentId) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = getById(commentId);
        em.getTransaction().begin();

        Comment commentToUpdate = em.find(Comment.class, commentId);

        commentToUpdate.setMessage(foundComment.getMessage());
        commentToUpdate.setRating(foundComment.getRating()+1);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void decreaseRating(final Long commentId) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = getById(commentId);
        em.getTransaction().begin();

        Comment commentToUpdate = em.find(Comment.class, commentId);

        commentToUpdate.setMessage(foundComment.getMessage());
        commentToUpdate.setRating(foundComment.getRating()-1);

        em.getTransaction().commit();
        em.close();
    }

// 1. Добавить Entity "Tag". Поля: id + name
//    Добавить репозиторий
    //--------------------------------------

// 2. Добавить 2 метода в CommentRepository:
//       - void increaseRating(Long commentId);
//       - void decreaseRating(Long commentId);

// 3. Добавить метод в TagRepository позволяющий
//    сохранить сразу несколько тегов:
//    void save(List<Tag> tags);

// 4. Добавить метод в NewsRepository позволяющий
//    получить новости в определенный промежуток времени создания:
//    List<News> findByDate(Date from, Date until);

// 5. news к comments -> @OneToMany
//    user к comments -> @OneToMany
//    news к tags -> @ManyToMany
}
