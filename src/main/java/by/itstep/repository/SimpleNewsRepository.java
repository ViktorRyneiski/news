package by.itstep.repository;

import by.itstep.entity.Comment;
import by.itstep.entity.News;
import by.itstep.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static by.itstep.util.EntityManagerUtils.getEntityManager;

@Repository
public class SimpleNewsRepository implements NewsRepository {

    public List<News> findAll() {
        EntityManager em = getEntityManager();

        List<News> foundElements = em.createQuery("From News", News.class)
                .getResultList();

        em.close();
        return foundElements;
    }

    public News findById(Long id) {
        EntityManager em = getEntityManager();

        News foundNews = em.find(News.class, id);

        em.close();
        return foundNews;
    }

    @Override
    public News save(News news) {
        EntityManager em = getEntityManager();

        em.getTransaction().begin();

        em.persist(news); // <- сюда нужно отправить объект

        em.getTransaction().commit();
        em.close();

        return news;
    }

    @Override
    public void update(News news) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        News newsToUpdate = em.find(News.class, news.getId());
        newsToUpdate.setHeader(news.getHeader()); // WTF?!

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        News newsToDelete = em.find(News.class, id);

        em.remove(newsToDelete);

        em.getTransaction().commit();
        em.close();
    }

}
