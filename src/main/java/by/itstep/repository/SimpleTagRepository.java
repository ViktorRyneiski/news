package by.itstep.repository;

import static by.itstep.util.EntityManagerUtils.getEntityManager;
import by.itstep.entity.News;
import by.itstep.entity.Tag;
import by.itstep.util.EntityManagerUtils;

import java.util.List;
import javax.persistence.EntityManager;

public class SimpleTagRepository implements TagRepository {

    @Override
    public Tag getById(final Long id) {
        EntityManager em = getEntityManager();

        Tag foundTags = em.find(Tag.class, id);

        em.close();
        return foundTags;
    }

    @Override
    public List<Tag> findAll() {
        EntityManager em = getEntityManager();

        List<Tag> foundElements = em.createNativeQuery("SELECT * FROM tag")
                .getResultList();

        em.close();
        return foundElements;
    }

    @Override
    public void save(final Tag tag) {

        EntityManager em = getEntityManager();

        em.getTransaction().begin();

        em.persist(tag); // <- сюда нужно отправить объект

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void update(final Tag tag) {

        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        Tag tagToUpdate = em.find(Tag.class, tag.getId());
        tagToUpdate.setName(tag.getName()); // WTF?!

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Tag tagToDelete = em.find(Tag.class, id);

        em.remove(tagToDelete);

        em.getTransaction().commit();
        em.close();
    }
}
