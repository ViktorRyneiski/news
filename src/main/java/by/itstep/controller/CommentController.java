package by.itstep.controller;

import by.itstep.entity.Comment;
import by.itstep.entity.News;
import by.itstep.service.CommentService;
import by.itstep.service.NewsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.Date;
import java.util.List;

@Controller
public class CommentController {

    private final CommentService commentService;
    private final NewsService newsService;

    public CommentController(final CommentService commentService, final NewsService newsService) {
        this.commentService = commentService;
        this.newsService = newsService;
    }

    @GetMapping("/open-save-comment")
    public String openSaveComment(Model model) { // можно просто Ньюс ньюс, но это гимор
        model.addAttribute("comment", new Comment());
        return "create-comment";
    }

    @PostMapping("/save-comment")
    public String createComment(Comment comment) {
        commentService.save(comment);
        return "single-news";
    }

    @GetMapping("/single-comment/{id}")
    public String openSingleComment(@PathVariable Long id, Model model) {
        Comment foundComment = commentService.findById(id);
        model.addAttribute("news", foundComment);
        return "single-comment";
    }

    @GetMapping("/allComments")
    public String openMainPage(Model model) {

        List<Comment> foundComments = commentService.findAll();
        model.addAttribute("allComments", foundComments);

        return "single-news";
    }

    @GetMapping("test-create-comment")
    public String saveTestData() {

        News news1 = new News();
        news1.setHeader("Super-news");
        news1.setContent("Super-content");
        news1.setPublished(true);
        news1.setCreatedAt(new Date(2000, 1, 1));

        News news2 = new News();
        news2.setHeader("Bad-news");
        news2.setContent("Bad-content");
        news2.setPublished(false);
        news2.setCreatedAt(new Date(2010, 1, 1));

        News savedNews1 = newsService.save(news1);
        News savedNews2 = newsService.save(news2);

//        savedNews1.setId(null);
//        savedNews2.setId(null);
        Comment c1 = new Comment();
        c1.setRating(0);
        c1.setMessage("First");
        c1.setCreatedAt(new Date(2010, 1, 1));
        c1.setNews(savedNews1);

        Comment c2 = new Comment();
        c2.setRating(0);
        c2.setMessage("Second");
        c2.setCreatedAt(new Date(2010, 1, 1));
        c2.setNews(savedNews2);

        Comment c3 = new Comment();
        c3.setRating(0);
        c3.setMessage("Third");
        c3.setCreatedAt(new Date(2010, 1, 1));
        c2.setNews(savedNews1);

        commentService.save(c1);
        commentService.save(c2);
        commentService.save(c3);
        return "index";
    }
}
