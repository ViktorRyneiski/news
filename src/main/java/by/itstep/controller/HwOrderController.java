package by.itstep.controller;

import by.itstep.entity.HwCustomer;
import by.itstep.entity.HwOrder;
import by.itstep.service.HwOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

public class HwOrderController {

    @Autowired
    private HwOrderService hwOrderService;


    @GetMapping(("/hwOrder/{id}"))
    public String getById(@PathVariable Long id, Model model) {
        HwOrder foundHwOrder = hwOrderService.getById(id);
        model.addAttribute("order",foundHwOrder);
        return "single-order";
    }

    @GetMapping("/hwOrders")
    public String showAll(Model model){
        List<HwOrder> foundHwOrders = hwOrderService.findAll();
        model.addAttribute("orders",foundHwOrders);
        return "all-orders";
    }

    @GetMapping("/hwOrder/create-form")
    public String openCreateForm(Model model) {
        model.addAttribute("hwOrder",new HwOrder());
        return "create-hwOrder";
    }

    @PostMapping("/hwOrder/create")
    public String create(HwOrder hwOrder) {
        hwOrderService.save(hwOrder);
        return "redirect:/hwOrders";
    }

    
    @GetMapping("/hwOrder/update/{id}")
    public String openUpdatingPage(@PathVariable Long id, Model model) {
        HwOrder foundHwOrder = hwOrderService.getById(id);
        model.addAttribute("hwOrder",foundHwOrder);
        return "update-order";
    }

    @PostMapping("/hwOrder/update/{id}")
    public String update(HwOrder hwOrder) {
        hwOrderService.save(hwOrder);
        return "redirect:/hwOrders";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        hwOrderService.delete(id);
        return "redirect:/hwOrders";
    }
}
