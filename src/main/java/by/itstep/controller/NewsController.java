package by.itstep.controller;

import by.itstep.entity.Comment;
import by.itstep.entity.News;
import by.itstep.service.CommentService;
import by.itstep.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.Date;
import java.util.List;

@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @GetMapping("/open-save-news")
    public String openSaveNews(Model model){ // можно просто Ньюс ньюс, но это гимор
        model.addAttribute("news",new News());
        return "news/create-news";
    }
    @PostMapping("/save-news")
    public String createNews(News news){
        newsService.save(news);
        return "redirect:/main";
    }

    @GetMapping("/single-news/{id}")
    public String openSingleNews(@PathVariable Long id, Model model){
        News foundNews = newsService.findById(id);
        model.addAttribute("news",foundNews);
        model.addAttribute("commentToSave",new Comment());
        return "news/single-news";
    }

    @GetMapping("/main")
    public String openMainPage(Model model) {

        List<News> foundNews = newsService.findAll();
        model.addAttribute("allNews",foundNews);

        return "index";
    }

    @GetMapping("test-create")
    public String saveTestData(){
        News n = new News();
        n.setCreatedAt(new Date(2000,1,1));
        n.setHeader("Super-header");
        n.setContent("Super-content");
        int x = (int)(Math.random()*10);
        n.setImageUrl("https://loremflickr.com/300/200?random=" + x);
        newsService.save(n);
        return "index";
    }

//    @Autowired
//    private NewsService newsService;
//
//    @Autowired
//    private NewsRepository newsRepository;
//
//    @Autowired
//    private CommentRepository commentRepository;
//
//    @GetMapping("/main") // -> localhost:8080/main
//    public String openMainPage(Model model) {
//        System.out.println("Controller -> openMainPage");
//        List<News> foundNews = newsService.findAll();
//        model.addAttribute("allNews", foundNews);
//        return "index"; // -> templates/index.html
//    }
//
//    @GetMapping("/news/{id}") // -> localhost:8080/news/13
//    public String openSingleNewsPage(Model model, @PathVariable Long id) {
//        News foundNews = newsService.findById(id);
//        model.addAttribute("news", foundNews);
//        return "single-news"; // -> templates/single-news.html
//    }
//
//    @GetMapping("/test-create")
//    public String testCreate() {
//        int x = (int) (Math.random() * 100);
//        News toSave = new News(null, "Some-Header #" + x,
//                "Some-Content", new Date(2000, 1, 1),
//                false);    // LocalDateTime.now()
//
//        newsService.save(toSave);
//        return "index";
//    }
//
    @GetMapping("/test")
    public String test(){
        News news1 = new News();
        news1.setHeader("Super-news");
        news1.setContent("Super-content");
        news1.setPublished(true);
        news1.setCreatedAt(new Date(2000,1,1));

        News news2 = new News();
        news2.setHeader("Bad-news");
        news2.setContent("Bad-content");
        news2.setPublished(false);
        news2.setCreatedAt(new Date(2010,1,1));

        News savedNews1 = newsService.save(news1);
        News savedNews2 = newsService.save(news2);

//        savedNews1.setId(null);
//        savedNews2.setId(null);

        Comment c1 = new Comment();
        c1.setRating(0);
        c1.setMessage("First");
        c1.setCreatedAt(new Date(2010,1,1));
        c1.setNews(savedNews1);

        Comment c2 = new Comment();
        c2.setRating(0);
        c2.setMessage("Second");
        c2.setCreatedAt(new Date(2010,1,1));
        c2.setNews(savedNews2);

        Comment c3 = new Comment();
        c3.setRating(0);
        c3.setMessage("Third");
        c3.setCreatedAt(new Date(2010,1,1));
        c2.setNews(savedNews1);

        commentService.save(c1);
        commentService.save(c2);
        commentService.save(c3);

        return "redirect:/main";
    }
//
//    @GetMapping("/test-read")
//    public String testRead(){
//
//        System.out.println("==============testRead==================");
//        News foundNews = newsRepository.findById(1L);
//        System.out.println("NEWS: " + foundNews);
//        System.out.println("NEWS COMMENTS: " + foundNews.getComments());
//
//        return "index";
//    }


    //TODO OSMYSLIT'
    @PostMapping("/news/{newsId}/save-comment")
    public String saveComment(@PathVariable Long newsId, Comment comment){

        News foundNews = newsService.findById(newsId);
        comment.setNews(foundNews);
        commentService.save(comment);

        return "redirect:/single-news/" + newsId;
    }
}
