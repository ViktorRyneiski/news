package by.itstep.controller;

import by.itstep.entity.HwCustomer;
import by.itstep.service.HwCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.Date;
import java.util.List;

@Controller
public class HwCustomerController {

    @Autowired
    private HwCustomerService hwCustomerService;

//    • @GetMapping(“/posts”) -> Открывает страницу со списком всех постов
    @GetMapping("/hwCustomers")
    public String showAllHwCustomers(Model model) {
        List<HwCustomer> foundHwCustomers = hwCustomerService.findAll();
        model.addAttribute("hwCustomers", foundHwCustomers);
        return "hwCustomer/all-hwCustomers";
    }

    //    • @GetMapping(“/post/{id}”) -> Открывает конкретный пост по ID
    @GetMapping("/hwCustomer/{id}")
    public String getById(@PathVariable Long id, Model model) {
        HwCustomer foundHwCustomers = hwCustomerService.getById(id);
        model.addAttribute("hwCustomer", foundHwCustomers);
        return "hwCustomer/single-hwCustomer";
    }

    //    • @GetMapping(“/post/create-form”) -> Открывает страницу с формой создания
    @GetMapping("/hwCustomer/create-form")
    public String createNewHwCustomer(Model model) {
        model.addAttribute("hwCustomer", new HwCustomer());
        return "hwCustomer/create-hwCustomer";
    }

    //    • @PostMapping(“/post/create”) -> Принимает пост и сохраняет его в БД
    @PostMapping("/hwCustomer/create")
    public String createHwCustomer(HwCustomer hwCustomer) {
        hwCustomerService.save(hwCustomer);
        return "redirect:/hwCustomers";
    }



    @GetMapping("/hwCustomer/update/{id}")
    public String openUpdatingPage(@PathVariable Long id, Model model) {
        HwCustomer foundHwCustomer = hwCustomerService.getById(id);
        model.addAttribute("hwCustomer", foundHwCustomer);

        return "hwCustomer/update-hwCustomer";
    }

    @PostMapping("/hwCustomer/update/{id}")
    public String update(HwCustomer hwCustomer) {
        hwCustomerService.update(hwCustomer);

        return "redirect:/hwCustomers";
    }

    @GetMapping("/hwCustomer/delete/{id}")
    public String delete(@PathVariable Long id){
        hwCustomerService.delete(id);
        return "redirect:/hwCustomers";
    }


//    @GetMapping("/hwCustomer/test-create")
//    public void createHwCustomer() {
//                HwCustomer hwCustomer = HwCustomer.getBuilder()
//                .firstName("Viktor")
//                .lastName("Ryneiski")
//                .address("Kalinovskogo 64/11")
//                .birthDate(new Date(1987,12,12))
//                .city("Minsk")
//                .state("DV")
//                .phone("80296649098")
//                .points(162)
//                .build();
//        hwCustomerService.save(hwCustomer);
//    }

}
